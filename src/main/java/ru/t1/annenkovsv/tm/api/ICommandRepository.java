package ru.t1.annenkovsv.tm.api;

import ru.t1.annenkovsv.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
