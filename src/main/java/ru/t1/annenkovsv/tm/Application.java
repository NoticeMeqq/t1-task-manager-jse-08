package ru.t1.annenkovsv.tm;

import ru.t1.annenkovsv.tm.constant.ArgumentConst;
import ru.t1.annenkovsv.tm.constant.CommandConst;
import ru.t1.annenkovsv.tm.constant.InformationConst;
import ru.t1.annenkovsv.tm.util.MemoryCalculationUtil;
import ru.t1.annenkovsv.tm.model.Command;
import ru.t1.annenkovsv.tm.repository.CommandRepository;
import ru.t1.annenkovsv.tm.api.ICommandRepository;
import java.util.Scanner;


public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("** ENTER COMMAND **");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    private static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument.toUpperCase()) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showMemoryInfo();
                break;
            default:
                showErrorArgument();
                break;
        }
        System.exit(0);
    }

    private static void processCommand(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument.toUpperCase()) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.INFO:
                showMemoryInfo();
                break;
            case CommandConst.EXIT:
                showExit();
            default:
                showErrorCommand();
                break;
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println("Current task manager commands and arguments list:");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer name: %s\n", InformationConst.DEVELOPERNAME);
        System.out.printf("Email: %s\n", InformationConst.EMAIL);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("Current program version: %s\n", InformationConst.CURRENTVERSION);
        System.out.printf("Last updated: %s\n", InformationConst.LASTUPDATED);
    }

    private static void showMemoryInfo() {
        System.out.println("[SYSTEM INFORMATION]");
        System.out.println("Available processors (cores): " + MemoryCalculationUtil.getAvailableProcessors());
        System.out.println("Free memory: " + MemoryCalculationUtil.getFreeMemory());
        System.out.println("Max memory: " + MemoryCalculationUtil.getMaxMemory());
        System.out.println("Total memory: " + MemoryCalculationUtil.getTotalMemory());
        System.out.println("Memory usage: " + MemoryCalculationUtil.getUsedMemory());
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("Input argument are not valid, please use arguments from list below:");
        System.err.printf("%s, %s, %s, %s\n", ArgumentConst.HELP, ArgumentConst.ABOUT, ArgumentConst.VERSION, ArgumentConst.INFO);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("Input command are not valid, please use commands from list below:");
        System.err.printf("%s, %s, %s, %s, %s\n", CommandConst.HELP, CommandConst.ABOUT, CommandConst.VERSION, CommandConst.INFO, CommandConst.EXIT);
    }

}